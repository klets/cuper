
require 'rails_helper'

describe 'navigate' do
  describe 'index' do
    it 'can be reached successfully' do
      visit posts_path
      expect(page.status_code).to eq(200)
    end

    it 'has a content' do
      visit posts_path
      expect(page).to have_content(/Esta es la index page/)
    end
  end

  describe 'creation' do
    before do
      user = User.create(email:"teste@mail.com", password:"qwerty", password_confirmation:"qwerty", tag:"luisa1")
      login_as(user, :scope => :user)
      visit new_post_path
    end

    it 'has a new form that can be reached' do
      expect(page.status_code).to eq(200)
    end

    it 'can be created from new form page' do
      fill_in 'post[date]', with: Date.today
      fill_in 'post[rationale]', with: "Algun_texto"
      click_on "Guardar"
      expect(page).to have_content("Algun_texto")
    end

    it 'will have a user associated it' do
      fill_in 'post[date]', with: Date.today
      fill_in 'post[rationale]', with: "User associating"
      click_on "Guardar"
      expect(User.last.posts.last.rationale).to eq("User associating")
    end
  end

end
