require 'rails_helper'

RSpec.describe User, type: :model do
  describe "creation" do
    before do
      @user = User.create(email:"teste@mail.com", password:"qwerty", password_confirmation:"qwerty", tag:"luisa1")
    end

    it "can be created" do
      expect(@user).to be_valid
    end

    it "cannot be created without tag" do
      @user.tag = nil
      expect(@user).to_not be_valid
    end
  end
end
