
# Cuper App

## Objetivo: registrar decisiones basadas en autoregulación cognitiva en hábitat compartidos.

## Models
- [ ] User -> Devise
- [ ] AdminUser -> Single Table Inheritance
- [ ] Post user's association
- [ ] Stat: attrs(2) ENUMS-way
- [ ] Place
- [ ] Common
- [ ] Share
- [ ] Outcome
- [ ] Transaction

## Características
- Admin dashboard
- Stats data con ENUM
- Wizard para la extensidad de las stats instances
- Actives jobs for GAME_core

## UI
- Bootstrap -> formateo
- Iconos sacados de Font_Awesome
- Adaptación del fomatos de las formas de wizard_stat

## Refactorizaciones

## Comandos
- `rake db:schema:load`
